package com.cybor.taskbookforann;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class MainActivity
        extends Activity
        implements View.OnClickListener, AdapterView.OnItemClickListener {
    private View warningContainer, loginContainer, errorContainer, closeQuizButton,
            quizInfoContainer, headerContainer;
    private ListView quizzesLV;
    private TextView titleTV, quizTextTV, quizAnswerET;
    private int quiz_id;
    private Random random;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        warningContainer = findViewById(R.id.warning_container);
        loginContainer = findViewById(R.id.login_container);
        errorContainer = findViewById(R.id.error_container);
        closeQuizButton = findViewById(R.id.close_quiz_button);
        quizInfoContainer = findViewById(R.id.quiz_info_container);
        headerContainer = findViewById(R.id.header_container);
        quizzesLV = (ListView) findViewById(R.id.quizzes_lv);
        titleTV = (TextView) findViewById(R.id.title_tv);
        quizTextTV = (TextView) findViewById(R.id.quiz_text_tv);
        quizAnswerET = (TextView) findViewById(R.id.quiz_answer_et);

        findViewById(R.id.agree_button).setOnClickListener(this);
        findViewById(R.id.yes_button).setOnClickListener(this);
        findViewById(R.id.no_button).setOnClickListener(this);
        findViewById(R.id.reply_button).setOnClickListener(this);
        closeQuizButton.setOnClickListener(this);

        random = new Random();

        quizzesLV.setAdapter(new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.quizes)
        ));
        quizzesLV.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.agree_button:
                warningContainer
                        .animate().alpha(0)
                        .withEndAction(() -> warningContainer.setVisibility(GONE));
                loginContainer.setVisibility(VISIBLE);
                loginContainer.animate().alpha(1);
                break;
            case R.id.yes_button:
                loginContainer
                        .animate().alpha(0)
                        .withEndAction(() -> loginContainer.setVisibility(GONE));
                quizzesLV.setVisibility(VISIBLE);
                headerContainer.setVisibility(VISIBLE);
                break;
            case R.id.no_button:
                loginContainer
                        .animate().alpha(0)
                        .withEndAction(() -> loginContainer.setVisibility(GONE));
                errorContainer.setVisibility(VISIBLE);
                errorContainer.animate().alpha(1);
                break;
            case R.id.close_quiz_button:
                quizInfoContainer
                        .animate().translationY(quizInfoContainer.getHeight())
                        .withEndAction(() -> quizInfoContainer.setVisibility(GONE))
                        .start();
                closeQuizButton.setVisibility(GONE);
                break;
            case R.id.reply_button:
                String userAnswer = quizAnswerET.getText().toString();
                String answer = getResources().getStringArray(R.array.answers)[quiz_id];
                String[] blocks = answer.split("\\|");
                boolean isCorrect = false;
                for (String block : blocks)
                    if (block.startsWith("cut"))
                        userAnswer = userAnswer.replace(block.split("cut")[0], "");
                    else isCorrect = isCorrect || answer.equals(block);
                if (isCorrect) {
                    String[] correctBlocks = getString(R.string.correct).split("!");
                    Toast.makeText(this, correctBlocks[random.nextInt(correctBlocks.length)], Toast.LENGTH_LONG).show();
                } else
                    errorContainer.setVisibility(VISIBLE);
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (quizInfoContainer.getVisibility() == GONE) {
            quizInfoContainer.setVisibility(VISIBLE);
            quizInfoContainer
                    .animate().translationY(0)
                    .start();
            closeQuizButton.setVisibility(VISIBLE);
        }
        Resources resources = getResources();
        titleTV.setText(resources.getStringArray(R.array.quizes)[position]);
        quizTextTV.setText(resources.getStringArray(R.array.quiz_texts)[position]);
        quiz_id = position;
    }
}